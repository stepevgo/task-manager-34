package ru.t1.stepanishchev.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.repository.ISessionRepository;
import ru.t1.stepanishchev.tm.api.service.ISessionService;
import ru.t1.stepanishchev.tm.model.Session;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull ISessionRepository repository) {
        super(repository);
    }

}