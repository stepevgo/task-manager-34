package ru.t1.stepanishchev.tm.repository;

import ru.t1.stepanishchev.tm.api.repository.ISessionRepository;
import ru.t1.stepanishchev.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}