package ru.t1.stepanishchev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.Task;

@NoArgsConstructor
public final class TaskCompleteByIdResponse extends AbstractTaskResponse {

    public TaskCompleteByIdResponse(@Nullable final Task task) {
        super(task);
    }

}